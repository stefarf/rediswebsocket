module gitlab.com/stefarf/rediswebsocket

go 1.19

require (
	github.com/gorilla/websocket v1.5.0
	github.com/labstack/echo/v4 v4.11.1
	gitlab.com/stefarf/automap v0.1.0
	gitlab.com/stefarf/iferr v0.1.1
	gitlab.com/stefarf/redisclient v0.0.0-20230723021200-e6bb2be6cb99
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/redis/go-redis/v9 v9.0.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.11.0 // indirect
	golang.org/x/net v0.12.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	golang.org/x/text v0.11.0 // indirect
)
