package rediswebsocket

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"gitlab.com/stefarf/automap"
	"gitlab.com/stefarf/iferr"
	"gitlab.com/stefarf/redisclient"
)

const automapThreshold = 0.5

var upgrader = websocket.Upgrader{} // use default options

type RedisWebSocket struct {
	mut           sync.Mutex
	channel       string
	redisInstance *redisclient.ClientWrapper
	pathConnMap   PathConnectionMap
	lastConnId    int64
}

type PathConnectionMap = *automap.Map[Path, ConnectionMap]
type ConnectionMap = *automap.Map[ConnId, *websocket.Conn]
type Path = string
type ConnId = int64

type PathMessage struct {
	Path    string
	Message json.RawMessage
}

func New(channel string, redisInstance *redisclient.ClientWrapper) *RedisWebSocket {
	rws := &RedisWebSocket{
		channel:       channel,
		redisInstance: redisInstance,
		pathConnMap:   automap.New[Path, ConnectionMap](automapThreshold),
	}
	go rws.consumeFromRedisAndWriteToWebSocket()
	return rws
}

func (rws *RedisWebSocket) HandleWS() echo.HandlerFunc {
	return func(c echo.Context) error {
		conn, err := upgrader.Upgrade(c.Response().Writer, c.Request(), nil)
		if err != nil {
			return err
		}
		defer func() { iferr.Print(conn.Close()) }()
		c.Set("websocket", true)

		path := c.Request().URL.Path

		rws.mut.Lock()

		connId := rws.lastConnId + 1
		rws.lastConnId = connId

		m, ok := rws.pathConnMap.Get(path)
		if !ok {
			m = automap.New[ConnId, *websocket.Conn](automapThreshold)
			rws.pathConnMap.Set(path, m)
		}
		m.Set(connId, conn)

		rws.mut.Unlock()

		defer func() {
			rws.mut.Lock()
			defer rws.mut.Unlock()
			m.Delete(connId)
		}()

		for {
			mt, message, err := conn.ReadMessage()
			if err != nil {
				return err
			}
			if mt != websocket.TextMessage && mt != websocket.BinaryMessage {
				return fmt.Errorf("unknown websocket message type: %d", mt)
			}
			if !json.Valid(message) {
				continue
			}
			iferr.Print(rws.redisInstance.PublishMarshal(rws.channel, PathMessage{
				Path:    path,
				Message: message,
			}))
		}
	}
}

func (rws *RedisWebSocket) consumeFromRedisAndWriteToWebSocket() {
	iferr.Fatal(rws.redisInstance.SubscribeAndConsume(
		[]string{rws.channel},
		func(channel, payload string) error {
			var am PathMessage
			err := json.Unmarshal([]byte(payload), &am)
			if err != nil {
				log.Println(err)
				return nil // do not disconnect
			}

			rws.mut.Lock()
			defer rws.mut.Unlock()
			m, ok := rws.pathConnMap.Get(am.Path)
			if !ok {
				return nil // do not disconnect
			}
			m.ForEach(func(_ ConnId, conn *websocket.Conn) (done bool) {
				iferr.Print(conn.WriteMessage(websocket.BinaryMessage, am.Message))
				return false
			})
			return nil // do not disconnect
		}))
}
